package three

import (
// "github.com/gopherjs/gopherjs/js"
)

// Constructor ==========================

// A Geometry holds all data necessary to describe a 3D model.
type Geometry struct {
	EventDispatcher
}

// Properties ===========================

// NewGeometry creates a new Geometry object
// The constructor takes no arguments.
func NewGeometry() *Geometry {
	return &Geometry{
		EventDispatcher: EventDispatcher{
			o: geometry.New(),
		},
	}
}

// ID is a unique number for this geometry instance.
func (g *Geometry) ID() int {
	return g.o.Get("id").Int()
}

// Name for this geometry. Default is an empty string.
func (g *Geometry) Name() string {
	return g.o.Get("name").String()
}

// Vertices holds every position of points in the model.
// To signal an update in this array, Geometry.verticesNeedUpdate needs to be set to true.
func (g *Geometry) Vertices() *Vertices {
	return &Vertices{
		g: g,
		o: g.o.Get("vertices"),
	}
}

// # .colors

// Array of vertex colors, matching number and order of vertices.
// Used in Points and Line.
// Meshes use per-face-use-of-vertex colors embedded directly in faces.
// To signal an update in this array, Geometry.colorsNeedUpdate needs to be set to true.

// Faces describe how each vertex in the model is connected with each other.
// To signal an update in this array, Geometry.elementsNeedUpdate needs to be set to true.
func (g *Geometry) Faces() *Faces {
	return &Faces{
		g: g,
		o: g.o.Get("faces"),
	}
}

// # .faceVertexUvs
// Array of face UV layers.
// Each UV layer is an array of UVs matching the order and number of vertices in faces.
// To signal an update in this array, Geometry.uvsNeedUpdate needs to be set to true.
// # .morphTargets

// Array of morph targets. Each morph target is a Javascript object:
// { name: "targetName", vertices: [ new THREE.Vector3(), ... ] } Morph vertices match number and order of primary vertices.
// # .morphNormals

// Array of morph normals. Morph normals have similar structure as morph targets, each normal set is a Javascript object:
// morphNormal = { name: "NormalName", normals: [ new THREE.Vector3(), ... ] }
// # .skinWeights

// When working with a SkinnedMesh, each vertex can have up to 4 bones affecting it. The skinWeights property is an array of weight values that correspond to the order of the vertices in the geometry. So for instance, the first skinWeight would correspond to the first vertex in the geometry. Since each vertex can be modified by 4 bones, a Vector4 is used to represent the skin weights for that vertex.
// The values of the vector should typically be between 0 and 1. For instance when set to 0 the bone transformation will have no affect. When set to 0.5 it will have 50% affect. When set to 100%, it will have 100% affect. If there is only 1 bone associated with the vertex then you only need to worry about the first component of the vector, the rest can be ignored and set to 0.
// # .skinIndices

// Just like the skinWeights property, the skinIndices' values correspond to the geometry's vertices. Each vertex can have up to 4 bones associated with it. So if you look at the first vertex, and the first skinIndex, this will tell you the bones associated with that vertex. For example the first vertex could have a value of ( 10.05, 30.10, 12.12 ). Then the first skin index could have the value of ( 10, 2, 0, 0 ). The first skin weight could have the value of ( 0.8, 0.2, 0, 0 ). In affect this would take the first vertex, and then the bone mesh.bones[10] and apply it 80% of the way. Then it would take the bone skeleton.bones[2] and apply it 20% of the way. The next two values have a weight of 0, so they would have no affect.
// In code another example could look like this:
// // e.g.
// geometry.skinIndices[15] = new THREE.Vector4(   0,   5,   9, 0 );
// geometry.skinWeights[15] = new THREE.Vector4( 0.2, 0.5, 0.3, 0 );

// // corresponds with the following vertex
// geometry.vertices[15];

// // these bones will be used like so:
// skeleton.bones[0]; // weight of 0.2
// skeleton.bones[5]; // weight of 0.5
// skeleton.bones[9]; // weight of 0.3
// skeleton.bones[10]; // weight of 0
// # .boundingBox

// Bounding box.
// { min: new THREE.Vector3(), max: new THREE.Vector3() }
// # .boundingSphere

// Bounding sphere.
// { radius: float }

// Set to true if the vertices array has been updated.
func (g *Geometry) VerticesNeedUpdate() bool {
	return g.o.Get("verticesNeedUpdate").Bool()
}
func (g *Geometry) SetVerticesNeedUpdate(value bool) {
	g.o.Set("verticesNeedUpdate", value)
}

// Set to true if the faces array has been updated.
func (g *Geometry) ElementsNeedUpdate() bool {
	return g.o.Get("elementsNeedUpdate").Bool()
}
func (g *Geometry) SetElementsNeedUpdate(value bool) {
	g.o.Set("elementsNeedUpdate", value)
}

// # .uvsNeedUpdate

// Set to true if the uvs array has been updated.
// # .normalsNeedUpdate

// Set to true if the normals array has been updated.
// # .colorsNeedUpdate

// Set to true if the colors array or a face3 color has been updated.
// # .groupdsNeedUpdate

// Set to true if a face3 materialIndex has been updated.
// # .lineDistancesNeedUpdate

// Set to true if the linedistances array has been updated.
// # .lineDistances

// An array containing distances between vertices for Line geometries. This is required for LinePieces/LineDashedMaterial to render correctly. Line distances can also be generated with computeLineDistances.

// Methods ================================

// EventDispatcher methods are available on this class.

// # .applyMatrix ( matrix )

// Bakes matrix transform directly into vertex coordinates.
// # .center ()

// Center the geometry based on the bounding box.
// # .rotateX ( radians )

// Rotate the geometry about the X axis. This is typically done as a one time operation, and not during a loop Use Object3D.rotation for typical real-time mesh rotation.
// # .rotateY ( radians )

// Rotate the geometry about the Y axis. This is typically done as a one time operation, and not during a loop Use Object3D.rotation for typical real-time mesh rotation.
// # .rotateZ ( radians )

// Rotate the geometry about the Z axis. This is typically done as a one time operation, and not during a loop Use Object3D.rotation for typical real-time mesh rotation.
// # .translate ( x, y, z )

// Translate the geometry. This is typically done as a one time operation, and not during a loop Use Object3D.position for typical real-time mesh translation.
// # .scale ( x, y, z )

// Scale the geometry data. This is typically done as a one time operation, and not during a loop Use Object3D.scale for typical real-time mesh scaling.
// # .lookAt ( vector )

// vector - A world vector to look at.
// Rotates the geometry to face point in space. This is typically done as a one time operation, and not during a loop Use Object3D.lookAt for typical real-time mesh usage.

// ComputeFaceNormals computes face normals.
func (g *Geometry) ComputeFaceNormals() {
	g.o.Call("computeFaceNormals")
}

// # .computeVertexNormals ( areaWeighted )

// areaWeighted - If true the contributution of each face normal to the vertex normal is weighted by the area of the face. Default is true.
// Computes vertex normals by averaging face normals.
// Face normals must be existing / computed beforehand.
// # .computeMorphNormals ()

// Computes morph normals.
// # .computeBoundingBox ()

// Computes bounding box of the geometry, updating Geometry.boundingBox attribute.

// ComputeBoundingSphere computes bounding sphere of the geometry, updating Geometry.boundingSphere attribute.
// Neither bounding boxes or bounding spheres are computed by default. They need to be explicitly computed, otherwise they are null.
func (g *Geometry) ComputeBoundingSphere() {
	g.o.Call("computeBoundingSphere")
}

// # .merge ( geometry, matrix, materialIndexOffset )

// Merge two geometries or geometry and geometry from object (using object's transform)
// # .mergeVertices ()

// Checks for duplicate vertices using hashmap.
// Duplicated vertices are removed and faces' vertices are updated.
// # .normalize ()

// Normalize the geometry.
// Make the geometry centered and has a bounding sphere whose radius equals to 1.0.
// # .clone ()

// Creates a new clone of the Geometry.
// This method copies only vertices, faces and uvs. It does not copy any other properties of the geometry.
// # .dispose ()

// Removes The object from memory.
// Don't forget to call this method when you remove a geometry because it can cause memory leaks.
// # .computeLineDistances ()

// Compute distances between vertices for Line geometries.
