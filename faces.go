package three

import (
	"github.com/gopherjs/gopherjs/js"
)

// Faces is an array of Face3 objects
type Faces struct {
	g *Geometry
	o *js.Object
}

// Push appends faces to this array
func (v *Faces) Push(faces ...*Face3) {
	jsFaces := make([]interface{}, len(faces))
	for i, v := range faces {
		jsFaces[i] = v.o
	}

	v.o.Call("push", jsFaces...)
}

// Update sets the elementsNeedUpdate flag on the associated Geometry
func (v *Faces) Update() {
	v.g.SetElementsNeedUpdate(true)
}
