package three

import (
// "github.com/gopherjs/gopherjs/js"
)

// Mesh is a base class for Mesh objects, such as MorphAnimMesh and SkinnedMesh.
type Mesh struct {
	Object3D
}

// Constructor =========================

// NewMesh constructs a new mesh object.
// geometry — An instance of Geometry.
// material — An instance of Material (optional).
func NewMesh(geometry *Geometry, material HasMaterial) *Mesh {
	return &Mesh{
		Object3D: Object3D{
			EventDispatcher: EventDispatcher{
				o: mesh.New(geometry.o, material.getMaterial().o),
			},
		},
	}
}

// Example

// var geometry = new THREE.BoxGeometry( 1, 1, 1 );
// var material = new THREE.MeshBasicMaterial( { color: 0xffff00 } );
// var mesh = new THREE.Mesh( geometry, material );
// scene.add( mesh );

// Properties =========================

// # .geometry

// An instance of Geometry, defining the object's structure.
// # .material

// An instance of Material, defining the object's appearance. Default is a MeshBasicMaterial with wireframe mode enabled and randomised colour.
// # .morphTargetInfluences

// An array of weights typically from 0-1 that specify how much of the morph is applied. Undefined by default, but reset to a blank array by updateMorphTargets.
// # .morphTargetDictionary

// A dictionary of morphTargets based on the morphTarget.name property. Undefined by default, but rebuilt updateMorphTargets.
// # .morphTargetBase

// Specify the index of the morph that should be used as the base morph. Replaces the positions. Undefined by default, but reset to -1 (non set) by updateMorphTargets.

// Methods ============================

// # .getMorphTargetIndexByName ( name )

// name — a morph target name
// Returns the index of a morph target defined by name.
// # .updateMorphTargets ()

// Updates the morphtargets to have no influence on the object. Resets the morphTargetForcedOrder, morphTargetInfluences, morphTargetDictionary, and morphTargetBase properties.
// # .raycast (raycaster, intersects)

// Get intersections between a casted ray and this mesh. Raycaster.intersectObject will call this method.
// # .clone (object)

// object -- (optional) Object3D which needs to be cloned. If undefined, clone method will create a new cloned Mesh Object.
// Clone a Mesh Object.
