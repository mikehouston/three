package three

import (
// "github.com/gopherjs/gopherjs/js"
)

// MeshNormalMaterial is a material that maps the normal vectors to RGB colors.
type MeshNormalMaterial struct {
	Material
}

// Constructor =======================

// NewMeshNormalMaterial constructs a new MeshNormalMaterial
func NewMeshNormalMaterial() *MeshNormalMaterial {
	return &MeshNormalMaterial{
		Material: Material{
			EventDispatcher: EventDispatcher{
				o: meshNormalMaterial.New(),
			},
		},
	}
}

// Properties ========================

// See the base Material class for common properties.
// # .wireframe

// Render geometry as wireframe. Default is false (i.e. render as smooth shaded).
// # .wireframeLinewidth

// Controls wireframe thickness. Default is 1.

// Due to limitations in the ANGLE layer, on Windows platforms linewidth will always be 1 regardless of the set value.
// # .morphTargets

// Define whether the material uses morphTargets. Default is false.
