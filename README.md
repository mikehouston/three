# Three.js bindings for GopherJS

This is still very early work. It currently wraps enough of the three.js object
model to allow meshes to be added to a scene, but setting up the renderer is still
handled in javascript.

I'll be adding more of the interface as I need it, so feel free to send pull requests
if you need parts that are missing. Needless to say, this library is NOT STABLE and may
change without warning.

## Example

~~~ go
package main

import (
	"bitbucket.org/mikehouston/three"
	"github.com/gopherjs/gopherjs/js"
)

func main() {
	scene := three.SceneFromJS(js.Global.Get("scene"))

	size := 32
	step := 50
	geometry := three.NewGeometry()
	for x := 0; x < size; x++ {
		for z := 0; z < size; z++ {
			geometry.Vertices().Push(three.NewVector3(
				float64(x*step-size*step/2),
				float64(z*z-x*x),
				float64(z*step-size*step/2),
			))

			if x > 0 && z > 0 {
				p1 := z + x*size
				p2 := (z - 1) + x*size
				p3 := (z - 1) + (x-1)*size
				p4 := z + (x-1)*size

				geometry.Faces().Push(three.NewFace3(p1, p2, p3))
				geometry.Faces().Push(three.NewFace3(p3, p4, p1))

				geometry.Faces().Push(three.NewFace3(p3, p2, p1))
				geometry.Faces().Push(three.NewFace3(p1, p4, p3))
			}
		}
	}
	geometry.ComputeFaceNormals()

	material := three.NewMeshNormalMaterial()
	mesh := three.NewMesh(geometry, material)
	scene.Add(mesh)
}
~~~