package three

import (
	"github.com/gopherjs/gopherjs/js"
)

// Vertices is an array of Vector3 objects
type Vertices struct {
	g *Geometry
	o *js.Object
}

// Push appends vectors to this array
func (v *Vertices) Push(vectors ...*Vector3) {
	jsVectors := make([]interface{}, len(vectors))
	for i, v := range vectors {
		jsVectors[i] = v.o
	}

	v.o.Call("push", jsVectors...)
}

// Update sets the veticesNeedUpdate flag on the associated Geometry
func (v *Vertices) Update() {
	v.g.SetVerticesNeedUpdate(true)
}
