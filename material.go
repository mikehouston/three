package three

import (
// "github.com/gopherjs/gopherjs/js"
)

// A Material describes the appearance of objects. They are defined in a
// (mostly) renderer-independent way, so you don't have to rewrite materials
// if you decide to use a different renderer.
type Material struct {
	EventDispatcher
}

// HasMaterial identifies sub-types of Material
type HasMaterial interface {
	getMaterial() *Material
}

func (m *Material) getMaterial() *Material {
	return m
}

// Properties

// # .id

// Unique number for this material instance.
// # .name

// Material name. Default is an empty string.
// # .opacity

// Float in the range of 0.0 - 1.0 indicating how transparent the material is. A value of 0.0 indicates fully transparent, 1.0 is fully opaque. If transparent is not set to true for the material, the material will remain fully opaque and this value will only affect its color.
// Default is 1.0.
// # .transparent

// Defines whether this material is transparent. This has an effect on rendering as transparent objects need special treatment and are rendered after non-transparent objects. For a working example of this behaviour, check the WebGLRenderer code.
// When set to true, the extent to which the material is transparent is controlled by setting opacity.
// Default is false.
// .blending

// Which blending to use when displaying objects with this material. Default is NormalBlending. See the blending mode constants for all possible values.
// .blendSrc

// Blending source. It's one of the blending mode constants defined in Three.js. Default is SrcAlphaFactor. See the destination factors constants for all possible values.
// # .blendDst

// Blending destination. It's one of the blending mode constants defined in Three.js. Default is OneMinusSrcAlphaFactor.
// # .blendEquation

// Blending equation to use when applying blending. It's one of the constants defined in Three.js. Default is AddEquation.
// # .depthTest

// Whether to have depth test enabled when rendering this material. Default is true.
// # .depthWrite

// Whether rendering this material has any effect on the depth buffer. Default is true.
// When drawing 2D overlays it can be useful to disable the depth writing in order to layer several things together without creating z-index artifacts.
// # .polygonOffset

// Whether to use polygon offset. Default is false. This corresponds to the POLYGON_OFFSET_FILL WebGL feature.
// # .polygonOffsetFactor

// Sets the polygon offset factor. Default is 0.
// # .polygonOffsetUnits

// Sets the polygon offset units. Default is 0.
// # .alphaTest

// Sets the alpha value to be used when running an alpha test. Default is 0.
// # .clippingPlanes

// User-defined clipping planes specified as THREE.Plane objects in world space. These planes apply to the objects this material is attached to. Points in space whose dot product with the plane is negative are cut away. Default is [].
// # .clipShadows

// Defines whether to clip shadows according to the clipping planes specified on this material. Default is false.
// # .overdraw

// Amount of triangle expansion at draw time. This is a workaround for cases when gaps appear between triangles when using CanvasRenderer. 0.5 tends to give good results across browsers. Default is 0.
// # .visible

// Defines whether this material is visible. Default is true.
// # .side

// Defines which of the face sides will be rendered - front, back or both.
// Default is THREE.FrontSide. Other options are THREE.BackSide and THREE.DoubleSide.
// # .needsUpdate

// Specifies that the material needs to be updated at the WebGL level. Set it to true if you made changes that need to be reflected in WebGL.
// This property is automatically set to true when instancing a new material.
// Methods

// EventDispatcher methods are available on this class.

// # .clone (material)

// material -- this material gets the cloned information (optional).
// This clones the material in the optional parameter and returns it.
// # .dispose ()

// This disposes the material. Textures of a material don't get disposed. These needs to be disposed by Texture.
// # .setValues (values)

// values -- a container with parameters.
// Sets the properties based on the values.
