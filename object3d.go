package three

import (
// "github.com/gopherjs/gopherjs/js"
)

// Object3D is the base class for scene graph objects.
type Object3D struct {
	EventDispatcher
}

// HasObject3D marks subtypes of Object3D
type HasObject3D interface {
	getObject3D() *Object3D
}

func (o *Object3D) getObject3D() *Object3D {
	return o
}

// Properties =========================

// # .id

// readonly – Unique number for this object instance.
// # .uuid

// UUID of this object instance. This gets automatically assigned, so this shouldn't be edited.
// # .name

// Optional name of the object (doesn't need to be unique).
// # .parent

// Object's parent in the scene graph.
// # .children

// Array with object's children.
// # .position

// Object's local position.
// # .rotation

// Object's local rotation (Euler angles), in radians.
// # .scale

// Object's local scale.
// # .up

// Up direction. Default is THREE.Vector3( 0, 1, 0 ).
// # .matrix

// Local transform.
// # .quaternion

// Object's local rotation as Quaternion.
// # .visible

// Object gets rendered if true.
// default – true
// # .castShadow

// Gets rendered into shadow map.
// default – false
// # .receiveShadow

// Material gets baked in shadow receiving.
// default – false
// # .frustumCulled

// When this is set, it checks every frame if the object is in the frustum of the camera. Otherwise the object gets drawn every frame even if it isn't visible.
// default – true
// # .matrixAutoUpdate

// When this is set, it calculates the matrix of position, (rotation or quaternion) and scale every frame and also recalculates the matrixWorld property.
// default – true
// # .matrixWorldNeedsUpdate

// When this is set, it calculates the matrixWorld in that frame and resets this property to false.
// default – false
// # .rotationAutoUpdate

// When this is set, then the rotationMatrix gets calculated every frame.
// default – true
// # .userData

// An object that can be used to store custom data about the Object3d. It should not hold references to functions as these will not be cloned.
// # .matrixWorld

// The global transform of the object. If the Object3d has no parent, then it's identical to the local transform.

// Methods ================================

// EventDispatcher methods are available on this class.

// # .applyMatrix ( matrix)

// matrix - matrix
// This updates the position, rotation and scale with the matrix.
// # .translateX ( distance )

// distance - Distance.
// Translates object along x axis by distance.
// # .translateY ( distance )

// distance - Distance.
// Translates object along y axis by distance.
// # .translateZ ( distance )

// distance - Distance.
// Translates object along z axis by distance.
// # .localToWorld ( vector )

// vector - A local vector.
// Updates the vector from local space to world space.
// # .worldToLocal ( vector )

// vector - A world vector.
// Updates the vector from world space to local space.
// # .lookAt ( vector )

// vector - A world vector to look at.
// Rotates object to face point in space.

// Add appends an object as child of this object. An arbitrary number of objects may be added.
// object - An object.
func (o *Object3D) Add(object ...HasObject3D) {
	jsobjects := make([]interface{}, len(object))
	for i, v := range object {
		jsobjects[i] = v.getObject3D().o
	}
	o.o.Call("add", jsobjects...)
}

// # .remove ( object, ... )

// object - An object.
// Removes object as child of this object. An arbitrary number of objects may be removed.
// # .traverse ( callback )

// callback - A function with as first argument an object3D object.
// Executes the callback on this object and all descendants.
// # .traverseVisible ( callback )

// callback - A function with as first argument an object3D object.
// Like traverse, but the callback will only be executed for visible objects. Descendants of invisible objects are not traversed.
// # .traverseAncestors ( callback )

// callback - A function with as first argument an object3D object.
// Executes the callback on this object and all ancestors.
// # .updateMatrix ()

// Updates local transform.
// # .updateMatrixWorld ( force )

// Updates global transform of the object and its children.
// # .clone ()

// Creates a new clone of this object and all descendants.
// # .getObjectByName (name)

// name -- String to match to the children's Object3d.name property.
// Searches through the object's children and returns the first with a matching name.
// # .getObjectById (id)

// id -- Unique number of the object instance
// Searches through the object's children and returns the first with a matching id.
// # .translateOnAxis (axis, distance)

// axis -- A normalized vector in object space.
// distance -- The distance to translate.
// Translate an object by distance along an axis in object space. The axis is assumed to be normalized.
// # .rotateOnAxis (axis, angle)

// axis -- A normalized vector in object space.
// angle -- The angle in radians.
// Rotate an object along an axis in object space. The axis is assumed to be normalized.
// # .raycast (raycaster, intersects)

// Abstract method to get intersections between a casted ray and this object. Subclasses such as Mesh, Line, and Points implement this method in order to participate in raycasting.
