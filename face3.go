package three

import (
	"github.com/gopherjs/gopherjs/js"
)

// Face3 is a triangle face
type Face3 struct {
	o *js.Object
}

// Example =============================

// var normal = new THREE.Vector3( 0, 1, 0 );
// var color = new THREE.Color( 0xffaa00 );
// var face = new THREE.Face3( 0, 1, 2, normal, color, 0 );

// Constructor =========================

// NewFace3 constructs a new Face3 object
// a — Vertex A index.
// b — Vertex B index.
// c — Vertex C index.
// normal — Face normal or array of vertex normals.
// color — Face color or array of vertex colors.
// materialIndex — Material index.
func NewFace3(a, b, c int) *Face3 {
	return &Face3{
		o: face3.New(a, b, c),
	}
}

// NewFace3Normal constructs a new Face3 object with a face normal, color and material
func NewFace3Normal(a, b, c int, normal *Vector3, color, materialIndex int) *Face3 {
	return &Face3{
		o: face3.New(a, b, c, normal.o, color, materialIndex),
	}
}

// NewFace3VertexNormals constructs a new Face3 object with vertex normals and vertex colors
func NewFace3VertexNormals(a, b, c int, normals [3]*Vector3, colors [3]int, materialIndex int) *Face3 {
	var jsnormals [3]*js.Object
	for i, v := range normals {
		jsnormals[i] = v.o
	}

	return &Face3{
		o: face3.New(a, b, c, jsnormals, colors, materialIndex),
	}
}

// Properties ==========================

// # .a

// Vertex A index.
// # .b

// Vertex B index.
// # .c

// Vertex C index.
// # .normal

// Face normal.
// # .color

// Face color.
// # .vertexNormals

// Array of 3 vertex normals.
// # .vertexColors

// Array of 3 vertex colors.
// # .materialIndex

// Material index (points to MultiMaterial.materials).
// Methods

// # .clone ()

// Creates a new clone of the Face3 object.
