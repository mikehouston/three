package three

import (
	"github.com/gopherjs/gopherjs/js"
)

// EventDispatcher binds JavaScript events for custom objects.
// https://github.com/mrdoob/eventdispatcher.js
type EventDispatcher struct {
	o *js.Object
}

// JSObject returns the underlying js.Object pointer for this object
func (e *EventDispatcher) JSObject() *js.Object {
	return e.o
}

// Methods ==============================

// # .addEventListener ( type, listener )

// type - The type of event to listen to.
// listener - The function that gets called when the event is fired.
// Adds a listener to an event type.
// # .hasEventListener ( type, listener )

// type - The type of event to listen to.
// listener - The function that gets called when the event is fired.
// Checks if listener is added to an event type.
// # .removeEventListener ( type, listener )

// type - The type of the listener that gets removed.
// listener - The listener function that gets removed.
// Removes a listener from an event type.
// # .dispatchEvent ( event )

// event - The event that gets fired.
// Fire an event type.
