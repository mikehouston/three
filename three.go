// Package three provides GopherJS bindings for the three.js project
// See http://threejs.org/
package three

import (
	"github.com/gopherjs/gopherjs/js"
)

var three = js.Global.Get("THREE")
var scene = three.Get("Scene")

var vector3 = three.Get("Vector3")
var face3 = three.Get("Face3")

var geometry = three.Get("Geometry")
var meshNormalMaterial = three.Get("MeshNormalMaterial")
var meshBasicMaterial = three.Get("MeshBasicMaterial")

var mesh = three.Get("Mesh")
