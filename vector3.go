package three

import (
	"github.com/gopherjs/gopherjs/js"
)

// Vector3 is a 3 dimensional vector
type Vector3 struct {
	o *js.Object
}

// Example ========================

// var a = new THREE.Vector3( 1, 0, 0 );
// var b = new THREE.Vector3( 0, 1, 0 );

// var c = new THREE.Vector3();
// c.crossVectors( a, b );

// Constructor ====================

// NewVector3 creates a new Vector3
// x -- Float the vector's x value
// y -- Float the vector's y value
// z -- Float the vector's z value
func NewVector3(x, y, z float64) *Vector3 {
	return &Vector3{
		o: vector3.New(x, y, z),
	}
}

// Properties =====================

// X returns the x coordinate of the vector
func (v *Vector3) X() float64 {
	return v.o.Get("x").Float()
}

// Y returns the y coordinate of the vector
func (v *Vector3) Y() float64 {
	return v.o.Get("y").Float()
}

// Z returns the z coordinate of the vector
func (v *Vector3) Z() float64 {
	return v.o.Get("z").Float()
}

// Methods ========================

// # .set ( x, y, z ) this

// Sets value of this vector.
// # .setX ( x ) this

// Sets x value of this vector.
// # .setY ( y ) this

// Sets y value of this vector.
// # .setZ ( z ) this

// Sets z value of this vector.
// # .copy ( v ) this

// Copies value of v to this vector.
// # .fromArray ( array, offset ) this

// array -- The source array in the form [x, y, z].
// offset -- An optional offset into the array.
// Sets the vector's components based on an array formatted like [x, y, z]
// # .add ( v ) this

// Adds v to this vector.
// # .addVectors ( a, b ) this

// Sets this vector to a + b.
// # .addScaledVector ( v, s ) this

// Adds the multiple of v and s to this vector.
// # .sub ( v ) this

// Subtracts v from this vector.
// # .subVectors ( a, b ) this

// Sets this vector to a - b.
// # .multiplyScalar ( s ) this

// Multiplies this vector by scalar s.
// # .divideScalar ( s ) this

// Divides this vector by scalar s.
// Set vector to ( 0, 0, 0 ) if s == 0.
// # .negate () this

// Inverts this vector.
// # .dot ( v ) this

// Computes dot product of this vector and v.
// # .lengthSq () this

// Computes squared length of this vector.
// # .length () this

// Computes length of this vector.
// # .lengthManhattan () this

// Computes Manhattan length of this vector.
// http://en.wikipedia.org/wiki/Taxicab_geometry
// # .normalize () this

// Normalizes this vector. Transforms this Vector into a Unit vector by dividing the vector by it's length.
// # .distanceTo ( v ) this

// Computes distance of this vector to v.
// # .distanceToSquared ( v ) this

// Computes squared distance of this vector to v.
// # .setLength ( l ) this

// Normalizes this vector and multiplies it by l.
// # .cross ( v ) this

// Sets this vector to cross product of itself and v.
// # .crossVectors ( a, b ) this

// Sets this vector to cross product of a and b.
// # .setFromMatrixPosition ( m ) this

// Sets this vector extracting position from matrix transform.
// # .setFromMatrixScale ( m ) this

// Sets this vector extracting scale from matrix transform.
// # .clamp ( min, max ) this

// min -- Vector3
// max -- Vector3
// If this vector's x, y or z value is greater than the max vector's x, y or z value, it is replaced by the corresponding value.

// If this vector's x, y or z value is less than the min vector's x, y or z value, it is replaced by the corresponding value.
// # .clampScalar ( min, max ) this

// min -- Float the minimum value the components will be clamped to
// max -- Float the maximum value the components will be clamped to
// If this vector's x, y or z values are greater than the max value, they are replaced by the max value.

// If this vector's x, y or z values are less than the min value, they are replaced by the min value.
// # .clampLength ( min, max ) this

// min -- Float the minimum value the length will be clamped to
// max -- Float the maximum value the length will be clamped to
// If this vector's length is greater than the max value, it is replaced by the max value.

// If this vector's length is less than the min value, it is replaced by the min value.
// # .floor () this

// The components of the vector are rounded downwards (towards negative infinity) to an integer value.
// # .ceil () this

// The components of the vector are rounded upwards (towards positive infinity) to an integer value.
// # .round () this

// The components of the vector are rounded towards the nearest integer value.
// # .roundToZero () this

// The components of the vector are rounded towards zero (up if negative, down if positive) to an integer value.
// # .applyMatrix3 ( m ) this

// m -- Matrix3
// Multiplies this vector times a 3 x 3 matrix.
// # .applyMatrix4 ( m ) this

// m -- Matrix4
// Multiplies this vector by 4 x 3 subset of a Matrix4.
// # .projectOnPlane ( planeNormal ) this

// planeNormal -- planeNormal A vector representing a plane normal.
// Projects this vector onto a plane by subtracting this vector projected onto the plane's normal from this vector.
// # .projectOnVector ( Vector3 ) this

// vector -- Vector3
// Projects this vector onto another vector.
// # .addScalar ( Float ) this

// s -- Float
// Adds a s to this vector.
// # .divide ( v ) this

// v -- Vector3
// Divides this vector by vector v.
// # .min ( v ) this

// v -- Vector3
// If this vector's x, y, or z value is greater than vector v's x, y, or z value, that value is replaced by the corresponding vector v value.
// # .max ( v ) this

// v -- Vector3
// If this vector's x, y, or z value is less than vector v's x, y, or z value, that value is replaced by the corresponding vector v value.
// # .setComponent ( index, value ) this

// index -- 0, 1, or 2
// value -- Float
// If index equals 0 the method sets this vector's x value to value
// If index equals 1 the method sets this vector's y value to value
// If index equals 2 the method sets this vector's z value to value
// # .transformDirection ( m ) this

// m -- Matrix4
// Transforms the direction of this vector by a matrix (a 3 x 3 subset of a Matrix4) and then normalizes the result.
// # .multiplyVectors ( a, b ) this

// a -- Vector3
// b -- Vector3
// Sets this vector equal to the result of multiplying vector a by vector b.
// # .getComponent ( index ) this

// index -- Integer 0, 1, or 2
// Returns the value of the vector component x, y, or z by an index.

// Index 0: x
// Index 1: y
// Index 2: z
// # .applyAxisAngle ( axis, angle ) this

// axis -- A normalized Vector3
// angle -- An angle in radians
// Applies a rotation specified by an axis and an angle to this vector.
// # .lerp ( v, alpha ) this

// v -- Vector3
// alpha -- Float between 0 and 1.
// Linear Interpolation between this vector and vector v, where alpha is the percent along the line.
// # .lerpVectors ( v1, v2, alpha ) this

// v1 -- Vector3
// v2 -- Vector3
// alpha -- Float between 0 and 1.
// Sets this vector to be the vector linearly interpolated between v1 and v2 with alpha factor.
// # .angleTo ( v ) this

// v -- Vector3
// Returns the angle between this vector and vector v in radians.
// # .setFromMatrixColumn ( index, matrix ) this

// index -- 0, 1, 2, or 3
// matrix -- Matrix4
// Sets this vector's x, y, and z equal to the column of the matrix specified by the index.
// # .reflect ( normal ) this

// normal -- Vector3 the normal to the reflecting plane
// Reflect incident vector off of plane orthogonal to normal. normal is assumed to have unit length.
// # .multiply ( v ) this

// v -- Vector3
// Multipies this vector by vector v.
// # .applyProjection ( m ) this

// m -- Matrix4 projection matrix.
// Multiplies this vector and m, and divides by perspective.
// # .applyEuler ( euler ) this

// euler -- Euler
// Applies euler transform to this vector by converting the Euler obect to a Quaternion and applying.
// # .applyQuaternion ( quaternion ) this

// quaternion -- Quaternion
// Applies a Quaternion transform to this vector.
// # .project ( camera ) this

// camera — camera to use in the projection.
// Projects the vector with the camera.
// # .unproject ( camera ) this

// camera — camera to use in the projection.
// Unprojects the vector with the camera.
// # .equals ( v ) this

// Checks for strict equality of this vector and v.
// # .clone () this

// Clones this vector.
// # .toArray ( array, offset ) this

// array -- An optional array to store the vector to.
// offset -- An optional offset into the array.
// Assigns this vector's x value to array[0].
// Assigns this vector's y value to array[1].
// Assigns this vector's z value to array[2].
// Returns the created array.
