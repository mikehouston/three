package three

import (
	"github.com/gopherjs/gopherjs/js"
)

// A Scene allows you to set up what and where is to be rendered by three.js. This is where you place objects, lights and cameras.
type Scene struct {
	Object3D
}

// Constructor =========================

// NewScene constructs a new scene object.
func NewScene() *Scene {
	return SceneFromJS(scene.New())
}

// SceneFromJS wraps a js.Object
func SceneFromJS(o *js.Object) *Scene {
	return &Scene{
		Object3D: Object3D{
			EventDispatcher: EventDispatcher{
				o: o,
			},
		},
	}
}

// Properties ==========================

// # .fog

// A fog instance defining the type of fog that affects everything rendered in the scene. Default is null.
// # .overrideMaterial

// If not null, it will force everything in the scene to be rendered with that material. Default is null.
// # .autoUpdate

// Default is true. If set, then the renderer checks every frame if the scene and its objects needs matrix updates. When it isn't, then you have to maintain all matrices in the scene yourself.
