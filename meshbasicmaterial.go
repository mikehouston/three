package three

import (
// "github.com/gopherjs/gopherjs/js"
)

// MeshBasicMaterial is a material for drawing geometries in a simple shaded (flat or wireframe) way.
// The default will render as flat polygons. To draw the mesh as wireframe, simply set the 'wireframe' property to true.
type MeshBasicMaterial struct {
	Material
}

// MeshBasicMaterialParams is an object with one or more properties defining the material's appearance.
type MeshBasicMaterialParams struct {
	color int //geometry color in hexadecimal. Default is 0xffffff.

	// map — Set texture map. Default is null
	// aoMap — Set ao map. Default is null.
	// aoMapIntensity — Set ao map intensity. Default is 1.
	// specularMap — Set specular map. Default is null.
	// alphaMap — Set alpha map. Default is null.
	// envMap — Set env map. Default is null.
	// combine — Set combine operation. Default is THREE.MultiplyOperation.
	// reflectivity — Set reflectivity. Default is 1.
	// refractionRatio — Set refraction ratio. Default is 0.98.
	// fog — Define whether the material color is affected by global fog settings. Default is true.
	// shading — Define shading type. Default is THREE.SmoothShading.
	// wireframe — render geometry as wireframe. Default is false.
	// wireframeLinewidth — Line thickness. Default is 1.
	// wireframeLinecap — Define appearance of line ends. Default is 'round'.
	// wireframeLinejoin — Define appearance of line joints. Default is 'round'.
	// vertexColors — Define how the vertices gets colored. Default is THREE.NoColors.
	// skinning — Define whether the material uses skinning. Default is false.
	// morphTargets — Define whether the material uses morphTargets. Default is false.
}

// Constructor =======================

// NewMeshBasicMaterial constructs a new MeshBasicMaterial
func NewMeshBasicMaterial(params *MeshBasicMaterialParams) *MeshBasicMaterial {
	return &MeshBasicMaterial{
		Material: Material{
			EventDispatcher: EventDispatcher{
				o: meshBasicMaterial.New(params),
			},
		},
	}
}
